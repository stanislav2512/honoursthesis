gulp.task('watch', function() {
    gulp.watch('./src/less/*.less', ['styles']);
    gulp.watch('./src/js/**', ['browserify']);
});