  $scope.initScene = function () {
                controls = OrbitControlsService.getControls(CameraService.getCamera(), document.getElementById('bds-threejs-container'));
                //create scene
                scene = new THREE.Scene();
                loadGroundTextures();
                createLights();
                createPoles();
                //create renderer
                renderer = new THREE.WebGLRenderer({antialias: true});
                renderer.setSize($scope.element.offsetWidth, $scope.element.offsetHeight);
                renderer.shadowMap.enabled = true;
                renderer.shadowMap.type = THREE.PCFSoftShadowMap;
                renderer.autoClear = false;
                //add the scene to HTML
                $scope.element.appendChild(renderer.domElement);
                //add Stats
                $scope.element.appendChild(StatsService.getStats().domElement);
                //reflection for a car
                sceneCube = new THREE.Scene();
                createEnvironment();
                //load the car
                CarService.getCar(scene, reflectionCube, controls);
            };