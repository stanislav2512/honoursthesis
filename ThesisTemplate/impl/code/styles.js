gulp.task('styles', function() {
    return gulp.src('./src/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('./dist/css/'))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/css/'));
});