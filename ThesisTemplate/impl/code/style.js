@darkblue: #031634;
@blue: #033649;
@darkterra: #CDB380;
@terra: #E8DDCB;

#bds-start-button {
	background-color: @blue;
	&:hover {
  		background-color: @terra;
  	a {
    	color: @blue;
  	}
}