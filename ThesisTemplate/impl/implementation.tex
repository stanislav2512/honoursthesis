\chapter{Implementation}\label{ch:Implementation}

This chapter examines the implementation of the project. The process of the modelling the 3D vehicle is described in detail, together with the importing methods to the 3D environment of the browser. Additionally, the functionality of the user interface, data binding and usage of AngularJS framework is discussed. Lastly, this chapter includes details about usage of three.js framework and its application in creating the 3D environment, receiving the input from the user and generating a simulation based on it.

\section{3D Modelling}

As the first part of the implementation phase, it was necessary to create a 3D model of a car. As described in the \nameref{ch:Design} chapter, prior to the modelling, author have obtained a high number of detailed reference pictures and blueprints of the vehicle. Blueprints were used to create reference walls (Figure \ref{fig:sub1}) to make sure that proper dimensions and shapes are maintained while modelling. The starting point of the modelling was the curve above the wheel as this can be created by a simple torus with everything but a part of its base removed. This base shape was then expanded further by extruding edges outwards and adjusting vertices to outline the reference picture (Figure \ref{fig:sub2}). Following the same principle, polygons were created to cover everything around the wheel and then curving the shape towards the front mask, modelling around lights and air intake (Figure \ref{fig:sub3}). At this point, the author realised that there was no bottom reference wall created, therefore, it was necessary to compare every extrusion, in the top view, to the reference picture, to make sure that the curve of the front mask is suitable. Only the right side of the mask was modelled as the symmetry modifier was used on the whole model to make sure that both right and left side of the car are identical.

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr10}
  \caption{Reference walls}
  \label{fig:sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr9}
  \caption{Initial modelling}
  \label{fig:sub2}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr8}
  \caption{Front mask modelling}
  \label{fig:sub3}
\end{subfigure}
\caption{Start of the modelling}
\label{fig:test}
\end{figure}

In certain parts of the model, especially around headlights and curves around the air intake, the extrusion of subsequent polygons did not provide enough vertices to create details. At that point, the big polygons around the detailed areas were cut into smaller ones and the cut edges were extended all the way across the model to make sure that the edge loops are consistent. Throughout the modelling process, NURBS smoothing was turned on and off to see how do the polygons transfer into a smooth surface. The smoothing results were satisfactory even though there were certain places where a complete smoothing was not desired. The front hood of the car has 2 places where the whole surface is raised rapidly, creating a sharper edge. These were achieved by chamfering particular edges and disabling smoothing for that area. Once the front mask shape was completed, the shape was extended towards the roof (Figure \ref{fig:sub4}) and towards the door (Figure \ref{fig:sub5}). Cars are not made out of one solid piece of metal, instead, they are built out of several smaller pieces. This is to allow the front hood of the car to be open and the same applies to the doors. Based on this it was necessary to split the model into different parts visually while the actual model stays as a single object. That was achieved by chamfering the edges that define the gaps and deleting the polygons between the edges created by the chamfer. Following this process, the split parts look as they are separated with a gap while staying within the same object. With the above-described procedures; extruding polygons, chamfering and sharpening the edges, and chamfering and removing the middle polygons the whole half of the car body was created (Figure \ref{fig:sub6}).

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr1}
  \caption{Roof modelling}
  \label{fig:sub4}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr2}
  \caption{Door modelling}
  \label{fig:sub5}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr3}
  \caption{Finished car body without details}
  \label{fig:sub6}
\end{subfigure}
\caption{Modelling continued}
\label{fig:test}
\end{figure}

The wheels of the car were created by joining a separately created tire and the rim. The tire was created by modelling the tire pattern with polygons, multiplying it along the line and then bending the created plate around a point (Figure \ref{fig:sub4}). Front air intake, rear view mirrors, head and rear lights were modelled separately too while being compared to the reference pictures and attached to the body object afterwards (Figure \ref{fig:sub5}). Once all the pair components were finished, the Symmetry modifier was applied definitely and the whole object was converted into an editable mesh to create one object. Lastly, front and back windows were created by applying bridge function to opposite edges on the window frame. The window polygons were then assigned a different material id, which prevented them from blending with the frame and created a nice separation instead. The whole model was finished by applying MeshSmooth modifier (Figure \ref{fig:sub6}). At this point, the final model had approximately 160 000 polygons and the size of the file, including textures, was 8.6 MB. This was not suitable for two reasons; the file size was too big to allow a quick load time, three.js found it difficult to handle such a high number of polygons, which was resulting in lower frame rate. Due to the aforementioned reasons the author had to reduce the number of polygons to a more reasonable number. That was achieved by removing unnecessary edge loops in places that did not provide any extra details with a higher number of polygons. The iteration level of the MeshSmooth modifier was lowered from 3 to 2 too. Eventually, the polygon count was cut down to approximately 17 000 with a file size of 3.2 MB. This file size was more suitable for quick import and animations with a high frame rate. 

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr4}
  \caption{Tire modelling}
  \label{fig:sub4}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr5}
  \caption{Detail modelling}
  \label{fig:sub5}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{impl/screenshots/scr6}
  \caption{Finished car model}
  \label{fig:sub6}
\end{subfigure}
\caption{Final part of modelling}
\label{fig:test}
\end{figure}

\section{Automation}

In the initial stage of the development phase, Gulp.js, an automation framework was used to create tasks, which would make the programming itself easier. The setup file consists of two main tasks. The first one is responsible for taking LESS and compiling it into CSS, compressing all CSS files into one and minifying it afterwards.

\lstinputlisting[caption={Sample Gulp Task},label={code:sampleTest}]{impl/code/styles.js}

In a similar way, the second task compresses all the JavaScript files into app.js and then applies Browserify framework to it. This framework takes care of dependencies and includes them in one file, to allow the creation of bundle.js. This is the only JavaScript file which is then loaded in the browser. Due to the testing issues (described in the \nameref{ch:Evaluation} chapter), Browerify currently does not actually resolve any dependencies as everything is loaded manually. It was left in the project for future purposes, as the code base will be modularized in the future.

Lastly, Gulp.js was used to set up watch tasks. These tasks are responsible for watching selected files and detecting changes. If the file changes, a relevant task is then run as a reaction to the change.

\lstinputlisting[caption={Watch Tasks},label={code:sampleTest}]{impl/code/watch.js}

\section{User Interface and AngularJS}

With the responsive design in mind, the User Interface was almost exclusively created using Bootstrap elements. UI was split into 5 major regions with their own responsibilities. Most of these regions were defined as separate directives with a custom HTML tag and a controller responsible for data input and output. Directives allowed the author to keep the index.html file very clean and split parts of HTML into separate files:

 \begin{itemize}
\item <bds-menu> element is made out of elements that are displayed as a header in the desktop mode and responsive menu in the mobile mode. Interaction with these elements is controlled by PropertiesController.
\item <bds-control-panel> element contains elements that build the main sidebar, which allows the user to set the properties of the simulation and the information input and output is also controlled by PropertiesController.
\item <bds-result-modal> builds a result modal element which is displayed after the simulation is finished. Information to be displayed in the model are provided by ResultModalController.
\item <bds-camera-controls> is displayed in the top left corner of the scene and consists of four buttons, which control what camera is used to display the scene. Camera selection is managed by CameraController.
\item Last element is not defined as a custom directive. It is just a wrapper container which contains three.js scene and all the behaviour related to displaying the environment is controlled by SceneController.
\end{itemize}

Controllers are only responsible for manipulating the data and passing it to services. Aside from the SceneContoller, none of the other controllers is responsible for anything else apart from input, validation and displaying the data. SceneController will be discussed further in the \nameref{ch:Three} sub-chapter.

As an example, if the user clicks on any surface button to select a surface which is in HTML described as:

\lstinputlisting[caption={Surface Button},label={code:sampleTest}, language=HTML]{impl/code/surface.html}

setConition method with relevant surface will be called to store the value in PropertiesService:

\lstinputlisting[caption={Method to set selected surface},label={code:sampleTest}]{impl/code/setSurface.js}

and the button will be set as active, as checkSelectedConition method will return true:

\lstinputlisting[caption={Method to highlight the selected surface button},label={code:sampleTest}]{impl/code/getSurface.js}

As described above, this is how the user input handles surface selection elements and the data is passed to be stored in the services for further calculations. Same applies to speed selection, weather selection and all the other elements. The PropertyControllers handles both the sidebar menu in full screen and the menu in responsive mode.

Services are responsible for variety of actions and contain the core functionality.:
 \begin{itemize}
\item PropertiesService receives data from PropertiesController and stores all of them in Simulation object, which is then passed around for other uses. This service also stores all the kinds of surfaces and conditions in one place, which makes the future expansion very easy.
\item CameraService stores all the logic related to the selection of different cameras. When a camera is selected, this service gets a camera index from the CameraController. With this index, chosen camera is set as current and instructions are provided to define how should the camera behave during animation (stay static, follow the target etc.).
\item StatsService is an extracted piece of code, that sets properties of the statistics display, which shows data about the simulation as frame rate and the amount of memory used.
\item OrbitControlService is another extracted module that defines the behaviour of the cameras when the user wants to pan across the scene or "look around".
\item PhysService and CarService involve all the maths related to the simulation and are the main controlling element responsible for generating and presenting the simulation. These will be discussed further in the \nameref{ch:Physics} sub-chapter.
\end{itemize}

\lstinputlisting[caption={Style definition for start button},label={code:LESS}]{impl/code/style.js}

Lastly, the style of the component was defined using LESS dynamic stylesheet language. LESS was chosen over CSS for multiple reasons. It allows the user to define variables like colours or margins, which then make the style more readable and easy to understand. LESS also allows nesting of elements which results in better control over defining styles for different states of the element, sub-elements etc. As mentioned in the automation chapter, LESS is then compiled into CSS, compressed and minified and loaded into the HTML document. As an example, style definition for the start button is shown in Listing \ref{code:LESS}.

\section{three.js}\label{ch:Three}

All the three.js objects used in this project are defined in either SceneController or CarService. SceneController serves mainly as a wrapper class as it contains all the objects that are displayed to the user in the 3D environment. Additionally, it stores car object loaded and provided by CarService. CarService loads the car model and handles all the animation and physics related to it.

\subsection{Scene Creation}

Once the user interface is loaded, SceneController takes care of loading the scene for the user. This is done by creating instances of necessary classes and setting up their properties. Two instances of THREE.Scene are created, the first one to serve as a container for all the objects to be displayed and the second one to store the environment which is then reflected on the surface of the car. THREE.WebGLRenderer instance is the most important one,  as it takes responsibility for rendering and presenting the scene to the user. This instance is created and appended to the DOM container, controlled by SceneController. Moreover, THREE.WebGLRenderer object stores properties as anti-aliasing, shadow rendering, shadow type and more. These need to be set upon creation. 

THREE.PerspectiveCamera is received from CameraService and its target is set to an instance of THREE.Car, which is created in CarService. Another 3 cameras are created and stored in the CameraService in order to be accessed quickly when the user is switching between the cameras using the user interface. Other parts of the scene are created by appending various objects like THREE.PlaneMesh, THREE.BoxMesh and THREE.DirectionalLight to the scene object. During the scene initiation, all the materials are also preloaded as either THREE.MeshPhongMaterial or THREE.MeshLambertMaterial objects. Materials are then directly applied to meshes or stored for future use, eg. when the user changes the surface.

Most importantly, SceneController contains animate and render functions that are called on every frame, to refresh the scene and the camera view. These functions also handle resizing the scene when the whole window is resized by calculating the aspect ratio from the window dimensions and redraw the scene accordingly.

When testing the responsiveness of the user interface, the author has experienced an unexpected behaviour, where if the scene's size was changed by resizing the window, the view was scaled only accordingly to the window size which resulted in squished models. This was caused by renderer calculating the aspect ratio based on the whole window dimensions instead of the container containing the scene only. The problem was resolved by replacing the source of dimensions for the aspect ratio of the scene with the container dimensions.

\lstinputlisting[caption={Initialize the scene},label={code:sampleTest}]{impl/code/initScene.js}

\subsection{Environment Creation} \label{ch:Environment}

An additional responsibility of SceneController is to create and control the environment displayed around the scene. Skybox in the environment is built by creating a box with very large dimensions and texturing the inside of it with THREE.ShaderMaterial. This material then receives a set of 6 images which are mapped to both positive and negative x, y and z values. 6 images used for texturing the skybox are achieved in the following way:

\begin{enumerate}
  \item Obtain an HDRI map with a good view of the sky and a variety of clouds. (http://hdrmaps.com/freebies was used for the purpose of this project).
  \item Create an empty Blender scene and apply the HDRI map to the scene environment.
  \item Create an animation with a camera which will move to 6 different positions (positive and negative x, y and z), take a picture and save it.
 \end{enumerate}

 The environment map is then displayed both as the sky in the scene and the reflection of the car. Additionally, the ground is created as a simple plane with a seamless texture applied. This texture changes based on the surface selected by the user. During the implementation phase, the author has encountered an issue where if a texture was applied to the ground panel, shadows cast by the car and other objects were not shown. This was caused by the fact that project was run on AMD graphics card and this was at that point generally known issue. The issue got resolved by upgrading the three.js library from r73 to r74 in late March 2016.

 Lastly, when in driving mode, users might experience ground texture flickering at certain angles. This is caused by floating point precision calculations errors when repeating seamless texture positions. This will be resolved in the future work.

\subsection{Car Import}

As mentioned in the \nameref{ch:Background} chapter, there is a variety of approaches when it comes to importing the car model into the environment. During the development of this project, the author was experimenting with both JSONLoader and OBJMTLLoader. The biggest concern with importing the car was the size of the file. When using OBJMTLLoader, the loader is passed paths for both .obj and .mtl files, which are then loaded into the browser. Within the browser, the .obj file is then parsed, and using regular expressions, vertices, normals and faces are extracted and calculated. These are then drawn afterwards using THREE.BufferGeometry. .mtl file is read in the same way and materials are added during the drawing process.

JSONLoader, on the other hand, takes completely different approach. As parsing .obj file in its native form in the browser is both time consuming and computationally expensive, JSONLoader takes a JSON file with more organised data as an input. This then means that the browser can read data and draw the model at the same time, as JSON data is significantly more organised. The major issue that the author had to face is casued by the fact that none of the high-performance 3D modelling software can export 3D models in JSON format. Luckily, the three.js community has provided a solution for this. The author of three.js provides a freely available Python script\cite{web:pScript} which takes .obj file as an input and outputs it as JSON. Another minor issue that was encountered with JSONLoader resulted in the textures not being applied properly. This was caused by Python script not handling .mtl files, which means that the model had only its original colours displayed.

The author has decided to use JSONLoader due to following reason:

\begin{itemize}
  \item 3D model stored in JSON file after applying the script provides a file, which is roughly 20\% smaller than a combined size of both .obj and .mtl files from the original model, resulting in faster loading time
  \item Data stored in JSON format makes it more readable and no in-browser processing is required once the file is loaded, resulting in shorter time required until the whole model is completely displayed.
  \item Even though OBJMTLLoader makes it easier to work with textures, all the other aspects make it slower and more cumbersome.
 \end{itemize}

 Choosing the JSONLoader means that the textures on the car were not rendering properly. This issue was overcome by manually accessing objects in the JSON file and setting the properties manually in the following way.

 \lstinputlisting[caption={Manual Texture Setup Example},label={code:sampleTest}]{impl/code/textures.js}

 This set up was done approximately with 12 textures with their own specific values. The car itself was then loaded by importing it as two separate objects; one for the car's body and one for its wheels, which were then duplicated and positioned appropriately.

 \lstinputlisting[caption={Car Import},label={code:sampleTest}]{impl/code/car.js}

\section{Physics and Simulation}\label{ch:Physics}

After the user sets all the properties using the user interface and presses "Start Simulation" button, PhysicsController takes care of all the calculations. As discussed in the \nameref{ch:Background} chapter the total stopping distance is calculated as a sum of thinking distance and braking distance. Using the formulas defined during research phase and user input values the total stopping distance is calculated and used both in the ResultModalController to display the results for the users. Moreover, deceleration value is calculated, using user's input, to be further used to produce the simulation.

 \lstinputlisting[caption={Calculation of Distances},label={code:sampleTest}]{impl/code/results.js}

 In order to be able to create a simulation using the imported 3D model, it was necessary to understand how does the THREE.Car object work. THREE.Car is a class which stores two 3D objects, a body and a wheel, which are used to build a car. Furthermore, it stores all the relevant properties about a car like maximum speed, acceleration, deceleration, body tilt, wheel rotation limit etc. It also stores information about the car's movement, direction, speed and more. The main purpose of this class is to provide the developer with a fully driving car, which can be driven using arrow controls. That is not a functionality which would fall into specifications of this project, therefore, the class had to be studied thoroughly and extended to provide the functionality necessary. Eventually, the class was changed in a way that it allows CarService to control if the car is moving, accelerating or stopping. 

 Once the user presses "Start Simulation" button, deceleration is calculated based on the coefficient of friction, this deceleration is then passed to the car object to enable braking at the suitable rate. Car's movement is set and afterwards, a process is followed to determine what is the current state of the car. This process is reiterated on every frame and is described in flow chart ({Figure~\ref{fig:using:flowchart}).

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.56\linewidth]{impl/screenshots/flowchart}
\caption{Simulation frame flowchart} \label{fig:using:flowchart}
\end{center}
\end{figure}

Simulation finishes by restarting the tilt of the car to its original position. This has to be done manually, as for some reason once the car stops, THREE.Car does not handle the tilt restart. The author thinks this is caused by altering the original class as its original purpose is not automated driving. This issue will be investigated further in the future work. After the finish of the simulation, the results are displayed to the user.

When testing the simulation it was discovered that the initially set up lights in the scene were not sufficient. Even though the whole scene was lit, the shadow map used for rendering the shadows was not big enough to cover the whole ground plane. Increasing the size of the shadow map was not a solution as the rendering became extremely computationally expensive. Placing multiple lights with their own shadow maps along the driving path was not possible either as three.js can currently only render up to 9 different lights and that high number of lights was also causing a lot of undesirable behaviour like over illumination and multiple shadows. The problem was temporarily solved by making the main light follow the car all the time, therefore, the shadow map is always around the car. Better solution is currently worked on and will be used in the future work.

\section{Conclusions}

In this chapter, the implementation phase of the project was shown, including 3D modelling of the car and techniques used, creation of user interface, the three.js scene, and the environment. Furthermore, it was discussed how is the researched physics used in the distance calculations based on the user input and then transferred into the actual simulation. All the technologies used were introduced and examples were shown of how do certain parts of the project work. Lastly, the simulation flowchart was provided to show how does the car accelerate, reaches maximum set speed and starts braking.

The fully functional 3D Braking Distance Simulator can be accessed at: \url{http://brakes.stanislavhamara.com}

Git repository with the source code can be accessed at (The repository does not include all the dependencies as those are loaded externally): \url{https://bitbucket.org/stanislav2512/honoursproject-brakingdistance3dsimulator/src}

Video of the demonstration usage can be found at: \url{https://youtu.be/N02Ph8Hu1sU}

