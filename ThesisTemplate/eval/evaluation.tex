\chapter{Evaluation \& Testing}\label{ch:Evaluation}

This chapter evaluates the overall project and provides results of tests carried out. Various methods of testing and their results are presented, including unit testing, UX testing and evaluation, and others.

\section{Software Unit Testing}

The intention of the author was to develop the software part of this project implementing Test Driven Development methods \cite{book:TDD} (write a failing test first, write functional code to make it pass, refactor the code and iterate trough these steps). By following the TDD methods it would be ensured that the whole code-base is fully covered by tests, and the author would have a full control over monitoring if later changes would interfere with parts that were implemented before.

Jasmine\cite{web:Jasmine} was used as a behaviour-driven JavaScript testing framework to write the tests and those were run afterwards using Karma\cite{web:Karma} test runner. As the PropertiesController and PropertiesService were implemented first, test cases were written to check if the values input by the user would be appropriately set in the service (Listing \ref{code:sampleTest}).

Unfortunately, as the codebase was growing bigger and bigger, the tests were struggling with various dependencies and eventually all of the structures had to be injected, which defeats the purpose of isolated testing of separate structures. Even thought the dependency issue was resolved, the unit testing was eventually stopped due to some of the dependencies needing properties from elements from DOM. As the web page is not rendered when the tests are run, the properties could not be accessed because the DOM elements were never defined.

\lstinputlisting[caption={PropertiesController Test Sample},label={code:sampleTest}]{eval/test.js}

The above issue could be solved by not injecting all the dependencies but creating their mocks with spies on the function instead. Those would then return the values necessary for the testing instead of getting them from the dependencies. At this point of the project the author had no experience with creating mocks for AngularJS services and their spies, thus, the unit testing was ruled out as out of scope for this project but will be returned to in the future work. This is a bad software development practice, but for the purpose of this project the final product was tested using multiple other methods which provided enough data to evaluate the project.

\section{User Experience Testing}

User Experience Testing of this project involved a questionnaire, that was sent to a large number of users to get quantitative data. As a part of the questionnaire, the user was given a set of instruction to perform certain actions using the web application. Afterwards, the user answered a set of question describing their experience with the usage of the application. These questions covered multiple areas as the performance of the application on different devices, load time of the application using different kinds of broadband and questions concerning the user interface, like the layout of elements, colour scheme and ease of use. The whole questionnaire can be found in Appendix \ref{appD}

The questionnaire provided a great amount of valuable information from the users and was afterwards used to do some minor changes to the user interface to provide better user experience. Initial few questions were focusing on the load time and graphical performance of the application. 68\% of the users describe their load time as fast or moderately fast. Approximately 52\% managed to run the application while maintaining 60 frames per second and only 4 users reported frame rate below 30 frames per second resulting in the average frame rate of 48 among all the users. As AMD graphics cards were causing shadows not to render before upgrading to the most recent version of three.js, users were required to report on this issue too. The majority of the users included the graphics card used in their device with a fair number of AMD branded ones. Feedback from the users has shown that 100\% could see the shadows, and hardware is not causing any issues anymore. Furthermore, general feedback on visual aspects of the web application was very positive, with users giving high marks to layout, colour scheme, component labelling and appearance of the environment.

There were certain alterations made to the user interface based mainly on the comment section of the questionnaire. Buttons controlling camera selection were relabelled from 1,2,3 to TOP, BACK, SIDE as this prevents the user from going trough all the options to find out which camera provides what angle. Switch button controlling the selection of units was relabelled from "Imperial Units" to "Speed Units" as many users found it difficult to switch from miles per hour to kilometres per hour. Additionally switch positions, originally as ON and OFF were changed to MPH and KM/H as this resulted in easier switching between speed units. Lastly conversion between the units was implemented, instead of setting the default value every time the user switches the units, as this feature was requested by many users in the comments.
section.

At the time of closing the questionnaire, 30 different users have submitted their feedback and this was deemed as sufficient for the purpose of this project.

\section{Result Testing}

One of the main purposes of the web application is to serve as an educational tool for learning drivers. It was, therefore, necessary to ensure that the results provided after the simulation are accurate, with only small errors. Testing was performed by calculating the stopping distance using the web application and then comparing the results with results provided by Physics Department at Georgia State University\cite{web:GSU}.

Sample testing results can be seen in the table below where the braking distances are compared. With the precision tolerance set to 2 m, the testing has shown that the precision error increases with the speed and braking distance. This is most likely caused by setting the Earth gravitation value to 9.81 in the implementation, whereas GSU's calculations use more precise value.

\begin{table}[H]
\centering
\label{my-label}
\begin{tabular}{llllll}
\textbf{Surface} & \textbf{Condition} & \textbf{Speed (km/h)} & \textbf{Expected (m)} & \textbf{Obtained (m)} & \textbf{Error (m)}           \\
Asphalt          & Dry                & 50                    & 14.06                        & 14.40                        & {\color[HTML]{009901} +0.34} \\
Asphalt          & Wet                & 50                    & 16.40                        & 16.80                        & {\color[HTML]{009901} +0.40} \\
Gravel           & Dry                & 70                    & 27.56                        & 28.22                        & {\color[HTML]{009901} +0.66} \\
Sand             & Dry                & 100                   & 87.48                        & 89.59                        & {\color[HTML]{CB0000} +2.11} \\
Ice              & Wet                & 80                    & 251.95                       & 258.03                       & {\color[HTML]{CB0000} +7.07} \\
Snow             & Dry                & 20                    & 5.25                         & 5.38                         & {\color[HTML]{009901} +0.13}
\end{tabular}
\caption{Physics testing results}
\end{table}

\section{Social, Ethical and Legal Evaluation}

The only legal issue, that needed to be resolved during this project was the prevention of copyright infringement during modelling of the vehicle. As mentioned in the \nameref{ch:Design} chapter, there were  certain actions undertaken to alter the visual appearance of the car, including removal of any branding and minor changes to the topology of the body mesh. Aside from that, all the software, libraries and frameworks used during the creation of the web application were either licensed by MIT license\cite{book:MIT} or used under student license for non-profit, educational purposes only.

From the social and ethical point of view and taking into consideration that this project is supposed to serve as an educational tool, it was ensured that the user is provided with accurate results. All the information output by the web application are designed not to set an unrealistic expectation for the driver in real life situation, to prevent any harm or damage caused to a person or property.

\section{Overall Evaluation}

During the background research, the author set three core aspects that were to be fulfilled in the final implementation of the project. First, the application is supposed to run in a native environment, not requiring the user to download any additional parts or plug-ins. This was achieved by using three.js as a WebGL library to render all the simulation parts and the scene with the environment. Secondly, a fully responsive design was proposed, to allow the user to run the web application on multiple different devices. This was achieved by designing the user interface using Bootstrap and tested by running the application on a variety of devices. Lastly, the author proposed an accurate visual simulation involving as accurate physics as possible. Using a variety of three.js classes and based on the feedback from the user questionnaire this was achieved sufficiently too.

\section{Conclusions}

In this chapter it was discussed, how was the final implementation tested. Testing involved unit testing, user experience testing using a questionnaire and result testing, to ensure that the user is provided with accurate results. Additionally, user feedback was analysed and it was described, what changes were made based on the quantitative results of the user experience testing. Social, ethical and legal issues were presented and paired with methods used to resolve those issues. Finally, it was summed up with evaluating if the three proposed core aspects of the project were fulfilled.
 
