  describe('speed control', function () {
        it('should change when the control button is pressed', function () {
            $scope.setSpeed(5);
            expect($scope.speed).toBe(45);
        });
    });

    describe('units control', function () {
        it('should change to imperial when turned on', function () {
            $scope.togglePreferences.imperial = true;
            $scope.setUnits();
            expect($scope.units).toBe('mph');
        });

        it('should change to metric when turned off', function () {
            $scope.togglePreferences.imperial = false;
            $scope.setUnits();
            expect($scope.units).toBe('km/h');
        });
    });