\chapter{Background Research}\label{ch:Background}

In this chapter, all the aspects that needed to be studied prior to the implementation of the project are discussed. In addition to that, existing solutions to this project's proposed problem are evaluated.

\section{Existing Solutions}

The variety of braking distance simulators publicly available online shows that the choice is quite poor. There are numbers of braking distance calculators that will provide the user with a right result based on user's input but none of them can actually provide a visual representation of how would the vehicle behave in real life.

The Royal Society for the Prevention of Accidents' Stopping Distances\cite{web:RoSPA} made by elucidate3D and Scootle's "It's a drag" simulation \cite{web:Scootle} were two simulators used as an example to point out what are some major issues with the existing solutions.

Both of the above simulators suffer from multiple problems. They are Flash based applications which require the user to download Flash plugin in order to be able to run the application. This is becoming a big issue nowadays as more and more browsers are shutting down the support for any 3rd party plugins. As both of the web pages are just empty containers with embedded Flash Player inside, neither of the pages have any responsive design properties. This makes it difficult for the user to open the application on a different device than a PC or a laptop. After quick testing, it was discovered that the user is unable to use the simulators both on a tablet or a mobile device. 

Lastly, the visual representation of a braking vehicle does not really correspond with the numbers that are calculated based on the user input. It is more of a moving image on a background than an actual simulation with all the physical properties.

Based on the study of the aforementioned simulators, three core aspects were pinpointed, that would be achieved in this implementation of the braking distance simulator, thus providing the user with slightly better experience when using the simulator. 

These three core aspects are: 
\begin{enumerate}
  \item Simulation running in a native environment, not requiring the user to download any additional parts
  \item Fully responsive design, allowing the user to use the simulator on a variety of devices
  \item Visually accurate simulation of a braking vehicle, including all the physics involved (i.e. suspension systems on a car)
 \end{enumerate}

\section{Importing 3D model into a browser}

One of the most challenging tasks of this project is to import finished 3D model into the browser. 3D models are developed using powerful pieces of software (Autodesk 3DS Max, Autodesk Maya, Blender etc.) that are tailored for manipulation with the 3D objects in their own format. Initially, in the first proposal, the plan for this project was to use some form of container that would be embedded in the web page, which would run a 3D environment to display and animate the 3D model of a vehicle. After initial research, it was decided that the project will use \textbf{Unity Web Player} as a 3D engine to run the embedded environment in the browser.

Throughout summer, this decision had to be changed due to the changes that were made by Google regarding plugin support in their browser. In September 2015, Chrome 45 removed the override and NPAPI support was  permanently removed from Chrome. Installed extensions that require NPAPI plugins are no longer able to load those plugins\cite{web:NPAPI}. As Unity Web Player falls into the category of aforementioned plugins, it was no longer possible to use it as a container to present the 3D environment.

The solution was eventually found by discovering \textbf{three.js} framework. Three.js is a cross-browser JavaScript library/API used to create and display animated 3D computer graphics in a web browser. Three.js uses WebGL. The source code is hosted in a repository on GitHub under MIT license\cite{web:threeLicense}. Three.js allows the creation of GPU-accelerated 3D animations using the JavaScript language as part of a website without relying on proprietary browser plugins. This is possible thanks to the advent of WebGL\cite{web:GPUAcceleration}.

As three.js has a very well documented API, active community and a variety of importing techniques, it was decided that it is going to be used as the 3D engine in this project.

There are multiple ways of importing the 3D model into the scene using three.js. This is done with the help of Loaders. According to the documentation, there are currently 17 different loaders implemented, each of them responsible either for importing the model, materials or both. The majority of these loaders can be ruled out immediately as they are file format specific and are not compatible with the formats used in this project. The rest of the loaders provide their own specific way of importing the 3D model and needed to be evaluated prior to their usage in this project.

3 most used loaders and 2 more experimental converters were looked at during the research phase:

 \begin{itemize}
  \item \textbf{JSONLoader} takes the 3D model as a file in JSON format. As the webpage is loaded, browser parses trough the file and renders the object according to the vertex coordinates stored in the JSON file.
  \item \textbf{OBJTMTLLoader} takes .obj and .mtl files as an input. These can be obtained trough exporting from Autodesk 3DS Max, Blender or any other 3D modelling software that allows the output of models in .obj format. The model is then transformed into text inside of the browser and parsed trough to be rendered.
  \item \textbf{ObjectLoader} is the newer version of JSONLoader and three.js developers are planing to deprecate JSONLoader once ObjectLoader reaches its final form. Unfortunately, there is a very low number of articles and tutorials that consider the usage of ObjectLoader at the moment.
  \item \textbf{UTF8 converter} is very experimental and even though it provides very good compression, it is, unfortunately, able to handle only one material and up to 64000 vertices.
  \item \textbf{FBX converter} is taking Autodesk's proprietary file format .fbx as an input. According to the developer community, this converter produces a lot of unnecessary JSON data and only very few features are actually implemented (material, UV mapping etc.).
 \end{itemize}

 The usage of a particular loader was not decided during the research phase of the project. The author experimented with both JSONLoader and OBJMTLLoader and those will be discussed further in Chapter 4, \nameref{ch:Implementation}.

\section{Vehicle Modelling Techniques}

Before the start of modelling of such a complex object as a car, it is necessary to investigate different techniques and good practices in order to create a nice mesh that can run smoothly in the browser without any deformations and additional artefacts. Making of 'Ford Mustang Fastback 65' tutorial\cite{web:MustangTutorial} was used as a core source of all the techniques used while modelling the car for this project.

A collection of reference pictures was collected before the start of the modelling from manufacturers webpage. To avoid any copyright issues, no branding was included in the actual model and the topology of the car was adjusted in few places to only resemble the original vehicle but not to be an exact copy of it.

One of the biggest challenges when it comes to vehicle modelling is a choice of modelling technique. As car models normally consist of very curved shapes and reflective materials, it is extremely important to have a clean mesh to prevent any reflection artefacts once the model is rendered. The decision was to be made between polygon modelling and NURBS modelling. NURBS modelling provides better results as the curves are mathematically calculated and provide more realistic reflections\cite{web:NURBS}. Unfortunately, there are two major issues that made use of NURBS almost impossible:
\begin{enumerate}
  \item It is possible to draw NURBS-based objects in three.js but this can only be done via code. When it comes to importing models to the environment, three.js is currently able to import polygon based meshes only.
  \item Author of this project has no prior experience with NURBS modelling and a model of a vehicle does not really classify as a starting point for learning NURBS modelling, thus, it was ruled out as out of the scope of this project.
 \end{enumerate}

 After ruling out NURBS modelling, polygon modelling was chosen as the way to go. As it is impossible to create a perfect curve using polygons it is important to find a balance between the quality of the model and polygon count. More polygons mean more detailed curves, on the other hand higher polygon count causes the size of the file to increase significantly.

 There are certain practices that can be followed to prevent creating reflection artefacts. It is important to realize that these practices affect both the cleanliness of the mesh and quality of the reflections, therefore, it is necessary to find an optimal balance between those two:\cite{web:practices}
 \begin{itemize}
  \item Using a smaller amount of vertices makes it possible to create flowing reflections and makes editing the mesh later on easier. At the same time, it subtracts from the smoothness of the model
  \item Proper  even spacing between edge loops improves flow of the reflection
  \item Surface should be cut or trimmed only once it has its final number of subdivision
  \item Support loops are not necessary, as this is not a character animation and no parts of the vehicle are going to be bending
 \end{itemize}

 If all the above techniques are applied, polygon modelling provides a highly enough detailed model which produces only small amount of reflection artefacts. For static rendering, these could be resolved in post processing and in animation environment they can be covered to a certain point by cameras, lights and skybox settings.


\section{Vehicle Braking Physics}

When designing an application that calculates the braking distance of a vehicle, a physics model needs to be defined to be used throughout all the calculations. As this thesis is not a part of a physics degree, it was  decided that only a simple physics model will be used in the calculations. That means that aspects as aerodynamics, engine braking or ABS are not be taken into consideration. That leaves us with total stopping distance being calculated as a combination of two elements; thinking distance and braking distance.

\textbf{Thinking} or \textbf{reaction distance} is the time that is necessary for the driver to realize that they need to step on the brakes after seeing an obstruction on the road. This distance can be influenced by many factors as tiredness, alcohol or drug consumption but in our model, we assume that driver is in a perfect state. Based on the GCSE Physics standard we will assume that the thinking distance is directionally proportional to the speed of the vehicle and for every 10mph increase in speed the distance goes by 3m\cite{web:GCSE}.

Thinking distance is then calculated as 

\begin{equation}
d_{PRT} = \frac{3v}{10}
\end{equation}

where
$\displaystyle d_{PRT}$ is driver's perception-reaction distance and $\displaystyle v$ is the velocity of the vehicle at the point of reaction.

\textbf{Braking distance} refers to the distance a vehicle will travel from the point when its brakes are fully applied to when it comes to a complete stop. It is primarily affected by the original speed of the vehicle and the coefficient of friction between the tires and the road surface\cite{book:brakingDistance}. In theory, braking distance could be determined by calculating the work required to reduce vehicle's kinetic energy to 0.

Based on the above assumption, where formula for kinetic energy of the vehicle is

\begin{equation}
E_{k} = \frac{1}{2}mv^{2}
\end{equation}

where $\displaystyle m$ is the weight of the vehicle and $\displaystyle v$ is the velocity of the vehicle and work required to stop the vehicle is

\begin{equation}
W = {\mu}mgd
\end{equation}

where $\displaystyle \mu$ is the coefficient of friction of a surface on which the vehicle is braking, $\displaystyle g$ is the gravity of Earth and $\displaystyle d$ is the distance travelled, braking distance can be calculated by putting the kinetic energy and work into an equation

\begin{gather*}
E_{k}=W\\
\frac{1}{2}mv^{2}={\mu}mgd
\end{gather*}

which results in

\begin{equation}
d_{BD}=\frac{v^{2}}{2g\mu}
\end{equation}

This is a very simplistic model for calculating the braking distance. It assumes that the friction force is only calculated using $\displaystyle g$ and $\displaystyle \mu$. This is not true in the real world and materials do behave differently at different speeds and with different masses of the vehicle. The $\displaystyle \mu$ also depends on if the wheels are completely locked or are still rolling\cite{rep:friction}. For the purpose of this project's simulations, it is assumed that the wheels are completely locked and the calculation is only dealing with surface friction, ignoring the rolling friction which would make the calculations significantly more complicated an is out of the scope of this project.

Finally the \textbf{total stopping distance} $\displaystyle d_{S}$ is calculated as a sum of \textbf{reaction distance} and \textbf{braking distance}

\begin{equation}
d_{S}=d_{PRT}+d_{BD}
\end{equation}

The last thing that needs to be defined in our model is the coefficient of friction. As there are no set standard values and the coefficients of friction vary depending on where you look (GSCE physics, mechanics, engineering or department of transport will all have different values), for the purpose of this project the values of coefficient of friction were set as follows:

\begin{table}[H]
\centering
\begin{tabular}{lll}
\multirow{2}{*}{\textbf{Surface}} & \multicolumn{2}{l}{\textbf{Condition}} \\
                                  & \textbf{Dry}       & \textbf{Wet}      \\
Asphalt                           & 0.7                & 0.6               \\
Gravel                            & 0.7                & 0.75              \\
Sand                              & 0.45               & 0.5               \\
Ice                               & 0.15               & 0.1              \\
Snow                              & 0.3                & 0.6              
\end{tabular}
\caption{Coefficients of friction}\label{tab:using:TableExample}
\end{table}


\section{Conclusions}

In this chapter existing solutions to the project's proposed problem were introduced and evaluated. Based on the existing solutions, three core aspects were pinpointed to be achieved in this project. Those aspects are native 3D environment without plugins, responsive design to work on multiple devices and visually accurate visualization of a breaking vehicle including all physics involved. In addition to that, three.js was picked as the 3D engine that will be used to render the 3D environment. Three.js was chosen due to its ability to run inside of the browser using WebGL without any additional plugins and its wide community with well documented API and examples.

Moreover different modelling techniques for vehicle modelling were described and compared. The outcome of the evaluation was a decision, where polygon modelling was chosen over NURBS to be used in modelling the 3D vehicle. Lastly, a simple physics model was defined to be used in all the calculations related to the braking distance. In this model, the braking distance is only based on vehicle's speed and coefficient of friction between the tires and the road surface. Other aspects as aerodynamics and rolling friction were ignored for simplicity reasons.

