\chapter{Design}\label{ch:Design}

This chapter examines the design of the project. Design progress of creating the model and the user interface is discussed in detail. Additionally, the initial design of the code base is presented and relationships between different elements are discussed.

\section{3D Model}

During the design phase of the car, the author was deciding between to vehicles that the project's model would be based on. These two vehicles were Ford Mustang 1969, which was originally modelled in the tutorial, that was used as the basis for all the modelling techniques used in this project, and Skoda Superb 2016. Eventually, Skoda Superb was chosen as the basis of the model. This was done, as the author wanted to apply new-learnt techniques to a completely new model, instead of just following a tutorial for an existing car. Moreover, the author believes that the chosen vehicle has more professional look and would help with emphasizing the fact that this project is supposed to serve as an educational tool.

The design is based on the blueprints ({Figure~\ref{fig:using:blueprints}), that can be freely obtained online\cite{web:blueprints}. The online source was also used for a collection of detailed images to aid with modelling detailed parts of the car as front lights, side-view mirrors and wheels.

In order to avoid copyright issues, it was necessary to alter the model in a way that it would only resemble the original vehicle, but it would not be an exact copy. All the branding was removed, specifically front mask of the car, back of the car and wheels only have blank plates instead of an actual logo. The edges and bevels on the bonnet of the car were smoothed out too, as the original model has a very recognizable front mask. Lastly, rims on the wheels were completely redesigned and the pattern on the rear lights was completely removed and replaced by a solid colour.

\begin{figure}[H]
\begin{center}
\includegraphics[width=1.05\linewidth]{design/images/blueprints1}
\caption{Skoda Superb 2016 Dimensions} \label{fig:using:blueprints}
\end{center}
\end{figure}

\section{User Interface}
\label{User Interface}

When creating the user interface it was important to focus both on the functional and visual part of the design. User should be presented with the user interface that is intuitive and easy to use while being visually appealing.

The basic wireframe was created using Balsamiq Mockups ({Figure~\ref{fig:using:balsamiq}). The biggest part of the screen was left out to be used as a container for the 3D environment. Header of the user interface was used for the title of the web application and it also provided enough room to be used as the menu, if the application was to be used on a tablet or a mobile device. Sidebar of the user interface was used as the main component for the user with all the inputs for speed, surface and weather conditions. Each of the above components was defined as a custom element in a separate file using AngularJS and a custom HTML tag was created to be used in the main document. This helped with keeping HTML clean and readable and all the elements could be edited without interfering with the others. 

\begin{figure}[H]
\begin{center}
\includegraphics[width=1.05\linewidth]{design/images/balsamiq}
\caption{User Interface Wireframe} \label{fig:using:balsamiq}
\end{center}
\end{figure}

During the design of the visual side of the user interface, it was necessary to select a colour scheme that would be suitable for this project. The choice of colours was chosen with a help of publicly available colour schemes for web design and these colours were then applied to Bootstrap components to create engaging, simplistic and responsive user interface. The whole design with the applied colour scheme can be found in Appendix \ref{appC}.

As Bootstrap was used as one of the main tools to build the user interface, its components provided an easy way of transforming the side panel into more compressed top panel ({Figure~\ref{fig:using:responsive}) when the application is open on devices with smaller screen.

\begin{figure}[H]
\begin{center}
\includegraphics[width=1.05\linewidth]{design/images/responsive}
\caption{Compressed panel for user input} \label{fig:using:responsive}
\end{center}
\end{figure}

The last decision concerning user interface to be made was a choice of the font. Sans Serif TrueType font from Ubuntu family was chosen as shown in the example: 

\setfont{pag}{The quick brown fox jumps over the lazy dog}.

\section{Functionality}

This project's functionality was implemented using mainly AngularJS and three.js frameworks. As AngularJS in this implementation does not really follow the conventional rules of OOP (class definition, inheritance etc.), but rather uses its own objects, it would be pretty difficult to represent the structure of the code base using classic UML diagrams. Author has decided to develop his own way of representing the relationship between particular objects an this is shown below ({Figure~\ref{fig:using:angular}).

\begin{figure}[H]
\begin{center}
\includegraphics[width=1.05\linewidth]{design/images/angularDiagram}
\caption{Application modules diagram} \label{fig:using:angular}
\end{center}
\end{figure}

As mentioned in the \nameref{User Interface} chapter, the user interface was split into 5 different sections: 3D environment, header, sidebar, camera controls and the result modal. Each of these sections, apart from the 3D environment, was defined as a custom HTML tag using AngularJS directives and these can be seen in the diagram highlighted in blue colour.

Each of the directives and the 3D environment has their own controller defined and assigned. All the controllers with the exception of SceneController are responsible only for handling user input, passing it to Services for further usage and responding back to the user by displaying the changes that have been made. SceneController serves as a wrapper class that encapsulates various three.js objects and by adding them together creates and renders the 3D environment.

There are 4 core services that provide the main functionality of the web application. PropertiesService is responsible for storing user input, PhysicsService calculates the stopping distance based on the user input, CameraService manipulates user's view of the environment and responds to switching between cameras and CarService contains all the functionality for importing the vehicle model, animation of the car and the actual simulation. Additional 2 services, StatsService and OrbitControlService, were created only to extract setup of FPS display and "look around" functionality into separate files.

Further use and more detailed description of the above structures will be discussed in the \nameref{ch:Implementation} chapter.

\begin{figure}[H]
\begin{center}
\includegraphics[width=1.05\linewidth]{design/images/classDiagram}
\caption{Class diagram} \label{fig:using:class}
\end{center}
\end{figure}

As the last part of the design phase, a class diagram ({Figure~\ref{fig:using:class}) was created to determine relationships between various three.js classes. Majority of the classes are linked directly to the THREE.Scene object as this is a collection class, that stores all of the elements contained in the scene including cameras, lights and visible objects. THREE.Mesh class will always have one instance of a material object (or a solid colour) and a geometry object passed in the constructor as it combines them together and creates a visible object. Another interesting case in the diagram is the relationship between THREE.Car and THREE.Clock classes. Car object needs a clock object in order to be able to generate the delta parameter, which is then used in all the calculations concerning car's animation. Lastly, groups of classes were surrounded by colour-coded regions to show where in the AngularJS module hierarchy are the classes located.

\section{Conclusions}

In this chapter all the steps of the design process were described. Design of the vehicle 3D model was described including all the visual changes that were made to prevent any copyright issues with the manufacturer of the vehicle that was used as the basis for the design of the car for this project. Design progress of the user interface was presented both from functional and visual point of view, discussing wireframes, layout, choice of elements and graphics design decisions like colour scheme and font. Moreover, code base structure was proposed using both structural and class diagrams, introducing both AngularJS structures like controllers, directives and services, and three.js classes responsible for creating and rendering objects in the 3D environment.


